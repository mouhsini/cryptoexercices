package org.dador;



import org.abstractj.kalium.crypto.Aead;
import org.abstractj.kalium.crypto.Random;
import org.abstractj.kalium.keys.KeyPair;
import static org.abstractj.kalium.encoders.Encoder.HEX;
import static org.abstractj.kalium.encoders.Encoder.RAW;

import org.abstractj.kalium.NaCl.Sodium;


public class KaliumBindingAeadTest {


    public static void main(final String[] args) {
        Random myRandom = new Random();

        KeyPair kp = new KeyPair();
        Aead myEncryptor = new Aead(kp.getPrivateKey().toBytes());

        System.out.println(kp.getPrivateKey());

        byte[] myMessage = "Ceci est un message secret".getBytes();
        byte[] myData = "Destinataire public".getBytes();

        byte[] myNonce = myRandom.randomBytes(Sodium.CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);
        byte[] myCryptogram = myEncryptor.encrypt(myNonce, myMessage, myData);

        System.out.println("Nonce      : " + HEX.encode(myNonce));
        System.out.println("Cryptogram : " + HEX.encode(myCryptogram));

        byte[] decryptedMessage = myEncryptor.decrypt(myNonce, myCryptogram, myData);
        System.out.println("Decrypted  : " + RAW.encode(decryptedMessage));

        byte[] myFalseData = "DestInataire public".getBytes();
        byte[] decryptedMessageBis = myEncryptor.decrypt(myNonce, myCryptogram, myFalseData);

        System.out.println("Decrypted  : " + RAW.encode(decryptedMessageBis));
    }
}
